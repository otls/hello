const path = require('path');

const baseDir = path.join(process.cwd(), "src");
/** Package path */
const generatorPath = path.join(__dirname, "../", 'generator');
const generatorTemplatePath = path.join(__dirname, '../', 'generator', 'templates');
/** application paths */
const apppath = path.join(process.cwd(), 'src', 'app')
const servicePath = path.join(process.cwd(), 'src', 'services');
const routePath = path.join(process.cwd(), 'src', 'routes');
const helperPath = path.join(process.cwd(), 'src', 'helpers');
const middlewarePath = path.join(process.cwd(), 'src', 'middlewares');
const modelPath = path.join(process.cwd(), 'src', 'models');
const modulePath = path.join(process.cwd(), 'src', 'modules');
const requestPath = path.join(process.cwd(), 'src', 'requests');
const responsePath = path.join(process.cwd(), 'src', 'responses');
const dataFormatPath = path.join(process.cwd(), 'src', 'dataFormats');  

module.exports = {
    'baseDir': baseDir,
    'generatorPath': generatorPath,
    'generatorTemplatePath': generatorTemplatePath,
    'serviceFolderName': 'services',
    'routeFolderName': 'routes',
    'servicePath': servicePath,
    'apppath': apppath,
    'routePath': routePath,
    'helperPath': helperPath,
    'middlewarePath': middlewarePath,
    'modelPath': modelPath,
    'modulePath': modulePath,
    'requestPath': requestPath,
    'responsePath': responsePath,
    'dataFormatPath': dataFormatPath,
}
module.exports = `
import { Router } from "express";
import { Request } from "../app/Modules";
import {  } from "../app/Services";
import {  } from "../app/Requests";

const route = Router();
export default (app) => {

    app.use('/path, "{middleware}", route);

    /*Example*/
    // route.get(
    //     '/',
    //     Request.validate(new RequestValidation),
    //     Request.wrap(async (req, res, next) => {
    //         return res.send();
    //     })
    // );
}`
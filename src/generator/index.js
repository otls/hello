const generator = require('./Generator');
module.exports = async (moduleName, moduleCategory) => {
     
     switch (moduleCategory) {
          case 'service':
               generator(moduleCategory, moduleName, 'service')
               break;
          case 'route':
               generator(moduleCategory, moduleName, 'route')
               break;
          case 'response':
               generator(moduleCategory, moduleName, 'response')
               break;
          case 'request':
               generator(moduleCategory, moduleName, 'request')
               break;
          case 'module':
               generator(moduleCategory, moduleName, 'module')
               break;
          case 'helper':
               generator(moduleCategory, moduleName, 'helper')
               break;
          case 'middleware':
               generator(moduleCategory, moduleName, 'middleware')
               break;
          case 'dataFormat':
               generator(moduleCategory, moduleName, 'dataFormat')
               break;
          default:
               console.log('------------------------------------');
               console.log("No module");
               console.log('------------------------------------');
               break;
     }
}
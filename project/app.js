const App = require('./src/app/App');
var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static('storage'));


// route init
App.initRouting(app);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.status(404).json({
    message: "You must be lost!"
  })
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  throw err;
  return res.status(err.status || 500).json(err.message);
  res.render('error');
});

module.exports = app;

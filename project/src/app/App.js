// const Routes = require ('./Routes');
import * as Routes from './Routes';
import { TokenGetterMiddleware } from "./Middlewares";

export const initRouting = (app) => {
    app.use(async (req, res, next) => {
        const page =  parseInt(req.query.page);
        const limit = parseInt(req.query.limit);

        req.query.page = (isNaN(page) || (page < 0)) ? 0 : page;
        req.query.limit = (isNaN(limit) || limit < 0) ? 10 : limit;
        
        return next();
    })
    app.use(new TokenGetterMiddleware);
    for (const key in Routes) {
        if (Object.hasOwnProperty.call(Routes, key)) {
            const element = Routes[key];
            element(app);
        }
    }
    return app;
}
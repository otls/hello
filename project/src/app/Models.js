'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const env = process.env.NODE_ENV || 'development';
const db = {};
const appaths = require('../configs/paths');
const config = require(path.join(appaths.CONFIGPATH, 'database'))[env];

let sequelize;
if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
    sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
    .readdirSync(appaths.MODELPATH)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        const model = require(path.join(appaths.MODELPATH, file))(sequelize, Sequelize.DataTypes);
        db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
    db[modelName].paginate = async (query = {}, page = 0, limit = 10) => {
        const model = db[modelName];
        const offset = page * limit;


        const queryCount = Object.keys(query).reduce((bucket, key) => {
            if (!['order', 'attributes', 'include', 'limit'].includes(key)) {
                bucket[key] = query[key]
            }
            return bucket;
        }, {});

        query.limit = limit;
        query.offset = offset;

        const count = await model.count(queryCount);
        const totalPages = Math.ceil(count / limit);
        const rows = await model.findAll(query);

        return {
            data: rows,
            meta: {
                totalPages,
                totalRows: count,
                limit,
                currentPage: page + 1,
                start: offset
            }
        };
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;

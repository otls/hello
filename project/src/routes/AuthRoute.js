import { Router } from "express";
import { AuthService } from "../app/Services";
import { Request, Validation, Validator } from "../app/Modules";
import { LoginRequest } from "../app/Requests";

const route = Router();
export default (app) => {
    app.use('/auth', route);

    route.post(
        '/login',
        new Validator(new LoginRequest),
        new Request(async (req, res) => {
            let service = new AuthService();
            return await service.login(req.body.email, req.body.password);
        })

    );

    route.post(
        '/registration',
        () => null,
        new Request(async (req, res, next) => {
            let service = new AuthService();
            return await service.register(req.nody)
        })
    )
}
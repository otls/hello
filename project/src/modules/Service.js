import  * as Responses  from '../app/Responses';
export default class Service {
    constructor() {
        // delete Errors.Service;
    }
    set(name, data) {
        return new Responses[name](data);
    }
}
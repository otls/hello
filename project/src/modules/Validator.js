import { Forbidden, ValidationError } from "../app/Responses";
import * as yup from 'yup'
export default class Validator {
    constructor(validation) {
        return this.#validate(validation);
    }
    
    #validate(validation) {
        return async (req, res, next) => {
            if (await validation.authorized(req, res, next)) {
                const schema = yup.object().shape(await validation.rules(req, res, next));
                return await schema
                .validate(req.body)
                .then((result) => next())
                .catch((error) => {
                        return res
                            .status(422).json(new ValidationError(error));
                    })
            } else {
                const error = new Forbidden();
                return res.status(error.code).json(error);
            }
        }
    }
}

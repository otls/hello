import * as yup from 'yup';

/**
 * base class of request (validation)
 * it contains general schema of validation
 */

export default class Validation {
    constructor() {
        this.yup = yup;

        this.yup.addMethod(yup.string, 'unique', (args) => {
            return this.yup.string().test("unique", "${path} already exists", async (value) => {
                return await this.#unique(args.model, args.col, value, args.ignored);
            });
        });

        this.yup.addMethod(yup.mixed, 'file', (args) => {
            return this.yup.mixed().test('file', "${path} is not valid", (value) => {
                return this.#file(args.mimetypes, args.size, value);
            })
        });
    }

    async stdRequiredStringUnique(value, model, field) {
        // let model = require('../app/Models')[model];
        // model = model.findOne({where: {[field] : value}})
    }

    async beforeValidation(req, res, next) {
        return next();
    }

    async #unique(model, column, value, ignored = null, label = null) {
        let data = require(`../app/Models`)[model];
        return new Promise((resolve, reject) => {
            if (value === ignored) {
                return resolve(true);
            }
            return data
                .findOne({ where: { [column]: value } })
                .then(data => {
                    if (data) {
                        return resolve(false);
                    }
                    return resolve(true)
                })
                .catch(e => {
                    return resolve(true);
                })
        })
    }

    async #file(mimetypes = null, size = null, value) {
        if (mimetypes) {
            const validMimetypes = mimetypes.split(",");
            if (validMimetypes && !validMimetypes.includes(value.mimetype ?? null)) {
                return Promise.resolve(false);
            }
        }
        if (typeof size == 'number' && (value.size > size)) {
            return Promise.resolve(false);
        }
        return Promise.resolve(true);
    }
}


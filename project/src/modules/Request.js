import { Forbidden, ValidationError } from "../app/Responses";
import { STORAGEPATH } from "../configs/paths";
import * as yup from 'yup';
import multer from 'multer';
export default class Request {

    constructor(fn, send = true) {
        return this.#wrap(fn, send);
    }

    #wrap(fn, send = true) {
        return async (req, res, next) => {
            try {
                const result = await fn(req, res, next);
                return send
                    ? res.status(result.code || 200).json(result || null)
                    : result;
            } catch (error) {
                return res
                    .status(error.code || 500)
                    .json(error || 'Server error!');
            }
        }
    }
    
    /**
     *  wrapping request handler/route handler
     *  return a middleware funtion
     *  @param {function} fn
     *  @return response
     */
    static wrap(fn, send = true) {
        return async (req, res, next) => {
            try {
                const result = await fn(req, res, next);
                return send
                    ? res.status(result.code || 200).json(result || null)
                    : result;
            } catch (error) {
                return res
                    .status(error.code || 500)
                    .json(error || 'Server error!');
            }
        }
    }

    /**
     *  file uploading hanldle
     *  This is using multer
     *  set diskStorge and filter
     *  return a middleware funtion
     *  @param {../requests/} validation
     *  @param {node requests} req
     *  @param {node response} res
     *  @param {node next} next
     *  @return middlewareFunction
     */
    static async #fileValidation(validation, req, res, next) {
        // check whether this process is authorized (../requests/validation:authorized)
        if (! await validation.authorized(req, res, next)) {
            return res.status(403).json(new Forbidden);
        }
        // Get validation setup from validation (../requests/validation:file)
        let fileSettings = await validation.file(req, res, next);
        // Preapre storage configuration for multer
        const storage = multer.diskStorage({
            destination: (req, file, cb) => {
                const folder = fileSettings.folder ?? STORAGEPATH;
                cb(null, folder);
            },
            filename: (req, file, cb) => {
                const ext = file.mimetype.split("/");
                const name = fileSettings.filename ?? ext[0] + "_" + Date.now();
                cb(null, `${name}.${ext[1]}`);
            }
        });
        // Preapre storage configuration for multer
        const filter = (req, file, cb) => {
            const mimetype = file.mimetype.split("/")[1];
            if (fileSettings.mimetypes.includes(mimetype)) {
                cb(null, true);
            } else {
                cb(new multer.MulterError('LIMIT_UNEXPECTED_FILE', file.fieldname), false);
            }
        }
        // Preapre default limitation for multer
        let limits = {
            fileSize: 3145728,
            files: 5
        }
        // get validation file limitation if any
        if (fileSettings.limits) {
            limits = { ...limits, ...fileSettings.limits };
        }
        // multer init
        let upload = multer({
            storage: storage,
            fileFilter: filter,
            limits: limits
        })[fileSettings.type](fileSettings.fieldName);
        // return multer 
        return upload(req, res, (err) => {
            if (err instanceof multer.MulterError) {
                return res.status(422).json(
                    new ValidationError(
                        {
                            path: err.field,
                            message: err.message
                        },
                        err.message
                    )
                );
            } else if (err) {
                return res.status(422).json(err);
            }
            return next();
        });
    }

    /**
     *  input validation for both formData / multipart / urlencoded
     *  This is using yup
     *  get the schema from ../request/validation*
     *  return the result of yup validation
     *  @param {../requests/} validation
     *  @param {node requests} req
     *  @param {node response} res
     *  @param {node next} next
     *  @return yupReturn
     */
    static async #inputValidation(validation, req, res, next) {
        // check whether this process is authorized (../requests/validation:authorized)
        if (await validation.authorized(req, res, next)) {
            // get schema from request vlidation (../request/validation:rules)
            const schema = await validation.rules(req, res, next);
            const YupVlidation = yup.object().shape(schema);
            // return yup validation result
            return YupVlidation
                .validate(req.body)
                .then((result) => {
                    return next();
                })
                .catch((error) => {
                    const errResult = new ValidationError(error);
                    return res.status(422).json(errResult);
                });

        } else {
            return res.status(403).json(new Forbidden);
        }
    }

    /**
     *  validation handler this triggering all validations above
     *  this is triggered by routes
     *  @param {../requests/} validation
     *  @return [middlewareFunction]
     */
    static validate(validation) {
        let layers = [];
        // prepare input validation
        const inputValidation = async (req, res, next) => {
            return await this.#inputValidation(validation, req, res, next);
        }
        // check if there any file validation
        if (validation.file) {
            const fileValidation = async (req, res, next) => {
                return await this.#fileValidation(validation, req, res, next);
            }
            layers.push(fileValidation);
        }
        layers.push(inputValidation);
        return layers;
    }
}

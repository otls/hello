
export default class TokenGetterMiddleware {

    constructor() {
        return this.handle;
    }

    async handle(req, res, next) {
        if (req.headers.authorization) {
            req.token = req.headers.authorization.split(" ").pop();
        }
        return next();
    }
}


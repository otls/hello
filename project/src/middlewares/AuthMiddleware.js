import { Jwt } from "../app/Helpers";
import { users } from "../app/Models";
import { Unauthenticated } from "../app/Responses";
export default class AuthMiddleware {

    constructor() {
        return this.handle
    }

    async handle(req, res, next) {
        try {
            let payload = Jwt.verify(req.token);
            const user = await users.findOne({
                where: { id: payload.uid, usr_email: payload.uMail }
            });
            if (user) {
                req.user = user;
                return next()
            }
        } catch (error) {
        }
        return res.status(401).json(new Unauthenticated);
        
    }
}


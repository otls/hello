export default class Success {
  constructor(data = {}) {
    for (const k in data) {
      if (Object.hasOwnProperty.call(data, k)) {
        const element = data[k];
        this[k] = element;
      }
    }
    
    this.code = data.code ?? 200;
    this.success = true;
  }
}

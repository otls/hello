export default class Unauthenticated extends Error {

    constructor(message = 'unauthenticated') {
        super();
        this.code = 401;
        this.message = message
        this.success = false;
    }

}
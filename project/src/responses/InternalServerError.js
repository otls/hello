export default class InternalServerError extends Error {
    constructor(message = "Woops.. we have some trouble with the server") {
        super();
        this.code = 500;
        this.message = message;
    }
}

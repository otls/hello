import { Jwt, Hash } from '../app/Helpers';
import { users } from "../app/Models";
import { Service } from '../app/Modules';

export default class AuthService extends Service {

    constructor() {
        super();
        this.userModel = users;
        this.jwt = Jwt;
        this.hash = Hash;
    }

    async login(email, password) {
        try {
            return new Promise(async (resolve, reject) => {
                const user = await this.userModel.scope('withPassword').findOne({ where: { usr_email: email } });
                if (user && await this.hash.compare(password, user.usr_password)) {
                    const payload = {
                        uid: user.id,
                        uMail: user.usr_email,
                        loginAt: new Date
                    }
                    const token = this.jwt.sign(payload);
                    return resolve(this.set('Success', {data: token}));
                }
                return reject(
                    this.set('Unauthenticated', 'email or password is incorrect')
                );
            })
        } catch (error) {
            return this.set('InternalServerError');
        }
    }

    async register(data) {
        try {
            return new Promise(async (resolve, reject) => {
                data = {
                    'usr_name': data.name,
                    'usr_email': data.email,
                    'usr_password': Hash.make(data.password),
                    'usr_phone': data.phone ?? null
                };
                let registered = await users.create(data);
                return resolve(this.set('Success', registered));
            });
        } catch (error) {
            return this.set('InternalServerError');
        }
    }
}

import { Validation } from "../app/Modules";

export default class LoginRequest extends Validation {

    constructor() {
        super();
    }

    async authorized(req, res, next) {
        return true;
    }

    async rules(req, res, next) {
        return {
            email: this.joi.not,
            password: this.requiredPassword,
        }
    }

    async beforeValidation(req, res, next) {
        
    }
}

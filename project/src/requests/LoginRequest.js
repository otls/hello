import { Validation } from "../app/Modules";

export default class LoginRequest extends Validation{

    constructor() {
        super();
    }

    async authorized(req, res, next) {
        return true;
    }

    async rules(req, res, next) {
        return {
            email: this.yup.string().required().max(100).email(),
            password: this.yup.string().required().max(255)
        }
    }
}

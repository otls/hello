class Api {

    #_response(res, data, message, success = true, code) {
        let response = {
            success: success
        };
        message ? response.message = message : null;
        data ? response.data = data : null;
        return res
            .status(code)
            .json(response);
    }
    
    async success(res, data = null, message = null, code = 200) {
        return this.#_response(res, data, message, true, code);
    }

    async error(res, message, code = 500, data = null) {
        return this.#_response(res, data, message, false, code);   
    }
}

export default new Api();

import Bcrypt from 'bcrypt';
import Config from '../configs/hash';

class Hash {
    async compare(hash, plaintext) {
        return await Bcrypt.compare(hash, plaintext);
  }

  async make(plaintext) {
    return await Bcrypt.hash(plaintext, Config.round);
  }
}

export default new Hash();

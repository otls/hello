import Jsonwebtoken from 'jsonwebtoken';
import JwtConfig from '../configs/jwt';

class Jwt {
    
    sign (payload) {
        const token = Jsonwebtoken.sign(payload, JwtConfig.secret);
        return token;
    }
    verify (token)  {
        return Jsonwebtoken.verify(token, JwtConfig.secret, (err, decoded) => {
            return err ? false : decoded;
        });
    }
}

export default new Jwt();
import Path from 'path'

const root = process.cwd();

module.exports = {
    'ROOTPATH': root,
    'CONFIGPATH': Path.join(root, 'src', 'configs'),
    'APPPATH': Path.join(root, 'src', 'app'),
    'MODELPATH': Path.join(root, 'src', 'models'),
    'REQUESTPATH': Path.join(root, 'src', 'requests'),
    'ROUTEPATH': Path.join(root, 'src', 'routes'),
    'SERVICEPATH': Path.join(root, 'src', 'services'),
    'STORAGEPATH': Path.join(root, 'storage')
}
const path = require('@otls/hellopress-bundle/src/configs/paths');
const generator = require(path.generatorPath);
const argv = process.argv;

// console.log(argv); return;


const serve = async () => {
    if (typeof argv[2] == 'undefined' || typeof argv[3] == 'undefined') {
        console.log('------------------------------------');
        console.log('Sorry but i don\'t know what to do');
        console.log('------------------------------------');
        return false;
    }

    const input = argv[2].split(':');
    const order = input[0];
    const moduleCategory = input[1];

    switch (order) {
        case 'generate':
            await generator(argv[3], moduleCategory);
            break;

        default:
            console.log("default");
            break;
    }
}

serve()
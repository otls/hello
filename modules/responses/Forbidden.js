export default class Forbidden extends Error {
    constructor(message = "You are forbidden!") {
        super();
        this.code = 403;
        this.message = message;
        this.success = false;
    }
}

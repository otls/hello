export default class Success {
  constructor(data) {
    data.message ? this.message = data.message : null;
    data.data ? this.data = data.data : null;
    this.code = data.code ?? 200;
    this.success = true;
  }
}


export default class ValidationError extends Error {

    constructor(errors, message = 'Unprocessable Entity') {
        super();
        this.code = 422;
        this.message = message
        this.success = false;
        this.errors = {
            [errors.path]: errors.message 
        }
    }

}
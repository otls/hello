export default class Request {
    #wrap(fn) {
        return async (req, res, next) => {
          try {
              const result = await fn(req, res, next);
              const code = result.code ?? 200;
              const data = result.data ?? result;
              res.status(code).json(data);
          } catch (error) {
              const code = error.code ?? 500;
              const data = error.data ?? error;
              res.status(code).json(data);
          }
      }
  }
}
